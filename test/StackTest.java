import static org.junit.Assert.*;
import ku.util.Stack;
import ku.util.StackFactory;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

/**
 * Stack JUnit Test Cases looking for bugs in 2 kinds of Stack.
 * @author Raksit Mantanacharu 5710546402
 *
 */
public class StackTest {
	private Stack stack;
	private int capacity = 5;
	@Before
	public void setUp() {
		// the factory makes 2 kinds of stacks, 0 and 1
		StackFactory.setStackType(1); // 0 is default
		
		stack = StackFactory.makeStack(capacity);
	}
	
	@Test
	public void newStackIsEmpty() {
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
		assertEquals(0, stack.size());
	}
	
	@Test
	public void testStackIsFull() {
		for(int i = 1; i <= capacity; i++) stack.push(i);
		assertTrue(stack.isFull());
	}
	
	@Test
	public void testStackSize() {
		for(int i = 1; i <= capacity-1; i++) stack.push(i);
		assertEquals(capacity-1, stack.size());
	}
	
	@Test(expected = IllegalStateException.class)
	public void testPushElementToFullStack() {
		for(int i = 1; i <= capacity; i++) {
			Integer value = new Integer(i);
			stack.push(value);
		}
		assertEquals(capacity, stack.size());
		stack.push(new Integer(6));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testPushNullElementToStack() {
		Assume.assumeTrue(stack.isEmpty());
		stack.push(null);
	}
	
	@Test
	public void testPeekStackSoStackSizeIsTheSame() {
		for(int i = 1; i <= capacity; i++) {
			Integer value = new Integer(i);
			stack.push(value);
		}
		Object peek1 = stack.peek();
		Object peek2 = stack.peek();
		assertSame(peek1, peek2);
	}
	
	@Test
	public void testPeekEmptyStack() {
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
		assertEquals(0, stack.size());
		assertEquals(null, stack.peek());
	}
	
	@Test(expected = java.util.EmptyStackException.class)
	public void testPopEmptyStack() {
		Assume.assumeTrue(stack.isEmpty());
		stack.pop();
	}
	
	@Test
	public void testStackSizeUpdateWhenPushTheElements() {
		for(int i = 1; i <= capacity; i++) {
			Integer value = new Integer(i);
			stack.push(value);
			assertSame( value, stack.peek());
		}
	}
	
	@Test
	public void testStackSizeUpdateWhenPopTheElements() {
		for(int i = 1; i <= capacity; i++) {
			Integer value = new Integer(i);
			stack.push(value);
		}
		for(int i = capacity; i >= 1; i--) {
			Integer value = new Integer(i);
			assertEquals(value, stack.pop());
		}
		assertEquals(0, stack.size());
	}
}
